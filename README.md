# Requerimientos Principales

* Instalacion de pipenv (Forma Global)
* Instalacion de Python 3.9
* Instalacion de PostgresSQL 12.6
* Postman o Insomnia para realizar peticiones POST

# Comandos de Instalación Ubuntu
* Instalacion de pipenv

```
sudo apt install software-properties-common
sudo add-apt-repository ppa:pypa/ppa
sudo apt update
sudo apt install pipenv
pipenv --version
```

* Instalacion de Python 3.9

* Predeterminar una version de Python en el sistema

```
ls /usr/bin/python (Para visualizar todas las versiones de python que tiene instalado el sistema)
sudo update-alternatives --set python /usr/bin/python3.6 1 (Ej) (En caso de que no aparezcan versiones a dejar por defecto, se deben setear y dar una prioridad)
sudo update-alternatives --set python /usr/bin/python3.4 2
sudo update-alternatives --config python ()
```

* Instalacion de PostgresSQL

```
sudo apt-get install libpq-dev
sudo apt update
sudo apt install postgresql postgresql-contrib
sudo -u postgres createuser --interactive (Importante: Con nombre de usuario del PC)
sudo -u postgres createdb nombre_usuario
```
(Aunque mas adelante por medio del comando psql, se abre la consola de PostgresSQL para la creacion de base de datos)

# Correr la API en LOCAL
* Descargar el proyecto utilizando SSH no HTTP
* Una vez descargado el proyecto, ingresar a él, abrir consola del VSC (o editor de preferencia)

```
pipenv --python 3.9 (Se crear el ambiente de desarrollo)
pipenv shell (Se inicializa el ambiente de desarrollo)
pipenv install -d (Se encarga de instalar dependencias)

```

* Crear .env basado en .env.example
* La variable de entorno:

```
DATABASE_URL=postgresql://usuarioDB:passwordDB@localhost:5432/nombreDB
```

Se encarga de realizar la conexion con la base de datos. (Importante setear bien los valores con respecto a la base de dato local)

* Para visualizar en forma de arbol las dependencias instaladas en tu ambiente de desarrollo

```
pipenv graph
```

* Para correr la API (Situarse en el ambiente de desarrollo)

```
python run.py
```

* Si todo sale bien por consola se visualizara lo siguiente

```
 * Serving Flask app "authentication" (lazy loading)
 * Environment: dev
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 102-933-438
```

# Realizar pruebas con Insomnia o Postman
* Agregar en los headers:

```
llave: x-api-kek valor: 436236939443955C11494D448451F
llave: Content-Type valor: application/json
```

* Metodos a utilizar:

```
GET: http://127.0.0.1:5000/api/v1/core/restricted
GET: http://127.0.0.1:5000/
POST: http://127.0.0.1:5000/api/v1/core/test
```

* Para poblar la base de datos y hacer pruebas, enviar datos en formato JSON por el metodo POST antes mencionado

```
"name": "Guillermo",
"last_name": "Morales",
"email": "gmail@gmail.com"
```

* Si todo sale bien, respondera con STATUS 200 y los mismos datos ingresados enviados en preview



