from os import environ
from dotenv import load_dotenv
from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from flask_marshmallow import Marshmallow
import psycopg2
from .config import config as app_config
db = SQLAlchemy()
ma = Marshmallow()

def create_app():
  load_dotenv()
  APPLICATION_ENV = get_environment()
  app = Flask(app_config[APPLICATION_ENV].APP_NAME)
  app.config.from_object(app_config[APPLICATION_ENV])
    
  app.config['SQLALCHEMY_DATABASE_URI'] = environ.get('DATABASE_URL')
  app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
  app.config['SQLALCHEMY_POOL_SIZE'] = 100
    
    
  db.init_app(app)
  ma = Marshmallow(app)
  CORS(app, resource={
    r"/*":{
        "origins":"*"
    }
})

  from .core.views import core as core_blueprint
  app.register_blueprint(core_blueprint, url_prefix='/api/v1/core')

  return app

def get_environment():
  return environ.get('APPLICATION_ENV') or 'development'
