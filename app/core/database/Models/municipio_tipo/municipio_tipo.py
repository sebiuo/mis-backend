# coding=utf-8

from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship

from ...common.base import Base


class Municipio_Tipo(Base):
    __tablename__ = 'municipio_tipo'

    id_tipo_municipio = Column(Integer, primary_key=True)
    nombre_tipo = Column(String)
    municipios = relationship("Municipio", back_populates="municipio_tipo")

    def __init__(self, nombre_tipo):
        self.nombre_tipo = nombre_tipo
