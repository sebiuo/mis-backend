# coding=utf-8

from datetime import datetime
from sqlalchemy import Column, String, Integer, Boolean, ForeignKey, Table, Float
from sqlalchemy.orm import backref, relationship
from sqlalchemy.sql.sqltypes import TIMESTAMP

from ...common.base import Base

class Promocion(Base):
    __tablename__ = 'promocion'

    id_promocion = Column(Integer, primary_key=True)
    estado = Column(Boolean, default=True)
    fecha_inicio = Column(TIMESTAMP(timezone=False), nullable= False, default = datetime.now())
    fecha_termino = Column(TIMESTAMP(timezone=False), nullable= False)
    procentaje = Column(Float, nullable= False)
    anunziao_id = Column(Integer, ForeignKey('anunziao.id_anuncio'))
    anunziao = relationship("Anunziao", back_populates="promociones")
   
    def __init__(self, fecha_inicio,fecha_termino, procentaje, anunziao):
        self.fecha_inicio = fecha_inicio
        self.fecha_termino = fecha_termino
        self.procentaje = procentaje
        self.anunziao = anunziao
