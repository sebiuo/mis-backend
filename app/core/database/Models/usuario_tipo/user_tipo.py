# coding=utf-8

from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship

from ...common.base import Base


class UserTipo(Base):
    __tablename__ = 'user_tipo'

    id = Column(Integer, primary_key=True)
    tipo = Column(String)
    users = relationship("User", back_populates="user_tipo")

    def __init__(self, tipo):
        self.tipo = tipo
