# coding=utf-8

from datetime import datetime
from sqlalchemy import Column, String, Integer, Boolean, ForeignKey, Table
from sqlalchemy.orm import backref, relationship
from sqlalchemy.sql.sqltypes import TIMESTAMP

from ...common.base import Base

tipo = Table(
    'categoria', Base.metadata,
    Column('id_anuncio',Integer, ForeignKey('anunziao.id_anuncio')),
    Column('tipo',Integer, ForeignKey('tipo.tipo_id'))
)
class Anunziao(Base):
    __tablename__ = 'anunziao'
    id_anuncio = Column(Integer, primary_key=True)
    fecha_creacion = Column(TIMESTAMP(timezone=False), nullable= False, default = datetime.now())
    monto = Column(String(10), nullable=True)
    titulo = Column(String(100))
    descripcion = Column(String(500))
    visualizaciones = Column(Integer, default=0)
    estado = Column(Boolean, unique=False, default=True)
    anunziao_comentario = relationship("Anunziao_Comentario", uselist=False, backref="owner")
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship("User", back_populates="anunziaos")
    categorias = relationship("Tipo", secondary = tipo, backref = backref('suscripcion', lazy = 'dynamic'))
    promociones = relationship("Promocion", back_populates="anunziao")
   
    def __init__(self, monto, titulo, descripcion, fecha_creacion, user):
        self.fecha_creacion = fecha_creacion
        self.monto = monto
        self.titulo = titulo
        self.descripcion = descripcion
        self.user = user
