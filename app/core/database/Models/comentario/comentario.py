# coding=utf-8

from datetime import datetime
from sqlalchemy import Column, String, Integer, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import TIMESTAMP

from ...common.base import Base


class Comentario(Base):
    __tablename__ = 'comentario'

    id_comentario = Column(Integer, primary_key=True)
    contenido_comentario = Column(String(500))
    fecha_comentario = Column(TIMESTAMP(timezone=False), nullable= False, default = datetime.now())
    estado = Column(Boolean, unique=False, default=True)
    anunziao_comentario_id = Column(Integer, ForeignKey('anunziao_comentario.id_comentario'))
    anunziao_comentario = relationship("Anunziao_Comentario", back_populates="comentarios")
    owner_id = Column(Integer, ForeignKey('user.id'))

    def __init__(self, fecha_comentario,contenido_comentario, anunziao_comentario, owner):
        self.fecha_comentario = fecha_comentario
        self.contenido_comentario = contenido_comentario        
        self.anunziao_comentario = anunziao_comentario
        self.owner = owner
