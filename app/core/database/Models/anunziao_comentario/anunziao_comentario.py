# coding=utf-8

from sqlalchemy import Column, String, Integer, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from ...common.base import Base


class Anunziao_Comentario(Base):
    __tablename__ = 'anunziao_comentario'

    id_comentario = Column(Integer, primary_key=True)
    estado = Column(Boolean, unique=False, default=True)
    owner_id = Column(Integer, ForeignKey('anunziao.id_anuncio'))
    comentarios = relationship("Comentario", back_populates="anunziao_comentario")
    
    #anunziao_id = Column(Integer, ForeignKey('anunziao.id'))
    #comentario = relationship("Anunziao", back_populates="comentarios")
    
   

    def __init__(self, owner):
        self.owner = owner