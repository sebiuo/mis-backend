# coding=utf-8

from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship

from ...common.base import Base


class JuntaVecinal(Base):
    __tablename__ = 'junta_vecinal'

    id = Column(Integer, primary_key=True)
    nombre = Column(String)
    users = relationship("User", back_populates="junta_vecinal")

    def __init__(self, nombre):
        self.nombre = nombre
