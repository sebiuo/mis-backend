# coding=utf-8

from sqlalchemy import Column, String, Integer, Boolean

from ...common.base import Base

class Admin(Base):
    __tablename__ = 'admin'
    id = Column(Integer, primary_key=True)
    estado = Column(Boolean, unique=False, default=True)
    email = Column(String(50), unique=True)
    password = Column(String(50))
   
    def __init__(self, email, password):
        self.email = email
        self.password = password