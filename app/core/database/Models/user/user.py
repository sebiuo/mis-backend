# coding=utf-8

from datetime import datetime
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean
from sqlalchemy.orm import backref, relationship
from sqlalchemy.sql.sqltypes import TIMESTAMP


from ...common.base import Base



class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    nombre = Column(String(50))
    apellido = Column(String(50))    
    email = Column(String(50), unique=True)
    telefono = Column(String(50))
    password = Column(String(50))
    estado = Column(Boolean, unique=False, default=True)
    fecha_inicio = Column(TIMESTAMP(timezone=False), nullable= False, default = datetime.now())
    junta_vecinal_id = Column(Integer, ForeignKey('junta_vecinal.id'))
    junta_vecinal = relationship("JuntaVecinal", back_populates="users")
    user_tipo_id = Column(Integer, ForeignKey('user_tipo.id'))
    user_tipo = relationship("UserTipo", back_populates="users")
    comentarios = relationship("Comentario", uselist=True, backref="owner")
    anunziaos = relationship("Anunziao", back_populates="user")

    def __init__(self, nombre, apellido, email, telefono, fecha_inicio, password, user_tipo, junta_vecinal):
        self.telefono = telefono
        self.fecha_inicio = fecha_inicio
        self.nombre = nombre
        self.apellido = apellido        
        self.email = email
        self.password = password
        self.user_tipo = user_tipo
        self.junta_vecinal = junta_vecinal
