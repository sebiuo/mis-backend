# coding=utf-8

from datetime import datetime
from sqlalchemy import Column, String, Integer, ForeignKey, Boolean, Table
from sqlalchemy.orm import backref, relationship
from sqlalchemy.sql.sqltypes import TIMESTAMP


from ...common.base import Base


tipo = Table(
    'municipio_junta_vecinal', Base.metadata,
    Column('municipio',Integer, ForeignKey('municipio.id_admin')),
    Column('junta_vecinal',Integer, ForeignKey('junta_vecinal.id'))
)

class Municipio(Base):
    __tablename__ = 'municipio'

    id_admin = Column(Integer, primary_key=True)
    nombre = Column(String(50))
    apellido = Column(String(50))    
    email = Column(String(50), unique=True)
    password = Column(String(50))
    fecha_sesion = Column(TIMESTAMP(timezone=False), nullable= False, default = datetime.now())
    estado = Column(Boolean, unique=False, default=True)    
    municipio_tipo_id = Column(Integer, ForeignKey('municipio_tipo.id_tipo_municipio'))
    municipio_tipo = relationship("Municipio_Tipo", back_populates="municipios")
    junta_vecinales = relationship("JuntaVecinal", secondary = tipo, backref = backref('suscripcion', lazy = 'dynamic'))

    def __init__(self, nombre, apellido, email, password, fecha_sesion, municipio_tipo):
        self.nombre = nombre
        self.apellido = apellido
        self.email = email
        self.password = password        
        self.fecha_sesion = fecha_sesion
        self.municipio_tipo = municipio_tipo
       
