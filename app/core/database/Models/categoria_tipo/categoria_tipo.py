# coding=utf-8

from sqlalchemy import Column, String, Integer, Boolean, ForeignKey
from sqlalchemy.orm import backref, relationship
from sqlalchemy.sql.schema import Table

from ...common.base import Base

class Tipo(Base):
    __tablename__ = 'tipo'
    tipo_id = Column(Integer, primary_key=True)
    tipo_name = Column(String)

