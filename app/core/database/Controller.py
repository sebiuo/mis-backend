from datetime import datetime, timedelta, date
from os import environ
from sqlalchemy import and_, log
from sqlalchemy.sql.elements import Null
from sqlalchemy.sql.expression import null
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask import json, jsonify, make_response
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.exc import IntegrityError

from app.core.database.Models.junta_vecinal.juntaVecinal import JuntaVecinal
from app.core.database.Models.user.user import User
from app.core.database.Models.usuario_tipo.user_tipo import UserTipo
from app.core.database.Models.comentario.comentario import Comentario
from app.core.database.Models.anunziao.anunziao import Anunziao
from app.core.database.Models.anunziao_comentario.anunziao_comentario import Anunziao_Comentario
from app.core.database.Models.categoria_tipo.categoria_tipo import Tipo
from app.core.database.Models.promocion.promocion import Promocion
from app.core.database.Models.admin.admin import Admin
from app.core.database.Models.municipio.municipio import Municipio
from app.core.database.Models.municipio_tipo.municipio_tipo import Municipio_Tipo


from .common.base import session_factory
from .common.base import Base

db = SQLAlchemy()
ma = Marshmallow()

class Controller:
  def __init__(self):
    pass

  def isAdmin(self, email, password):
    session = session_factory()
    Admin_query = session.query(Admin)    
    try:
      Admin_query.filter(and_(Admin.email.like(email), Admin.password.like(password))).one()    
      session.commit()            
      session.close()          
    except NoResultFound:
      session.close()
      return False
    except: 
      session.close()
      return False
    return True
  
  def isJuntaVecinal(self, email, password):
    session = session_factory()
    User_query = session.query(User)    
    isTrue = False
    try:
      obj_user = User_query.filter(and_(User.email.like(email), User.password.like(password))).one()
      if obj_user.user_tipo.tipo == 'junta_vecinal':
        isTrue = True
      session.commit()            
      session.close()          
    except NoResultFound:
      session.close()
      return isTrue
    except: 
      session.close()
      return isTrue
    return isTrue
    
  def crearUsuarioPriveliago(self,nombre, apellido, email, telefono, password, nombre_junta_vecinal):
    #acá necesito dar a la clase correspondiente el registro
    #evaluar si es tipo usuario admin o junta_vecinal para asi llamar a la clase correcta y guardar el registro
    
    session = session_factory()
    junta_vecinal_query = session.query(JuntaVecinal)
    todas_las_juntas_vecinales = junta_vecinal_query.all()
    juntas_vecinales = []
    esCorrectoNombreJuntaVecinal = False

    for junta_vecinal in todas_las_juntas_vecinales:
      juntas_vecinales.append(junta_vecinal.nombre)
      if(nombre_junta_vecinal == junta_vecinal.nombre):
        esCorrectoNombreJuntaVecinal = True

    if (esCorrectoNombreJuntaVecinal):
      # acá debo crear el usuario
      # 1.- primero debo buscar el tipo de junta vecinal por medio de una query
      # 2.- segundo debo buscar el tipo de usuario en este caso 'junta vecinal'
      # 3.- crear el usuario junta vecinal, con: tipo de usuario, junta vecinal pertenece y datos
      # 
      fecha_inicio = datetime.now()
      Obj_junta_vecinal = junta_vecinal_query.filter(JuntaVecinal.nombre == nombre_junta_vecinal).one()
      Obj_usuario_tipo_query = session.query(UserTipo).filter(UserTipo.tipo == 'junta_vecinal').one()
      nuevo_usuario = User(nombre, apellido, email, telefono, fecha_inicio, password, Obj_usuario_tipo_query, Obj_junta_vecinal)
      session.add(nuevo_usuario)
      session.commit()
      

      objToReturn = { "status": 'ok', "id" : nuevo_usuario.id }
    else:
      objToReturn = { 'status': 'error', 'message': 'el tipo de junta vecinal no existe', 'juntas_vecinales': juntas_vecinales}
    session.close()
    return jsonify(objToReturn)   

  def crearUsuario(self, nombre, apellido, email, telefono, password, nombre_junta_vecinal):                
    session = session_factory()
    junta_vecinal_query = session.query(JuntaVecinal)
    todas_las_juntas_vecinales = junta_vecinal_query.all()
    juntas_vecinales = []
    esCorrectoNombreJuntaVecinal = False

    for junta_vecinal in todas_las_juntas_vecinales:
      juntas_vecinales.append(junta_vecinal.nombre)
      if(nombre_junta_vecinal == junta_vecinal.nombre):
        esCorrectoNombreJuntaVecinal = True

    if (esCorrectoNombreJuntaVecinal):
      # acá debo crear el usuario
      # 1.- primero debo buscar el tipo de junta vecinal por medio de una query
      # 2.- segundo debo buscar el tipo de usuario en este caso 'junta vecinal'
      # 3.- crear el usuario junta vecinal, con: tipo de usuario, junta vecinal pertenece y datos
      # 
      fecha_inicio = datetime.now()
      Obj_junta_vecinal = junta_vecinal_query.filter(JuntaVecinal.nombre == nombre_junta_vecinal).one()
      Obj_usuario_tipo_query = session.query(UserTipo).filter(UserTipo.tipo == 'ciudadano').one()
      nuevo_usuario = User(nombre, apellido, email, telefono, fecha_inicio, password, Obj_usuario_tipo_query, Obj_junta_vecinal)
      session.add(nuevo_usuario)
      session.commit()

      objToReturn = { "status": 'ok', "id" : nuevo_usuario.id }
    else:
      objToReturn = { 'status': 'error', 'message': 'el nombre de junta vecinal no existe', 'juntas_vecinales': juntas_vecinales}
    session.close()
    return jsonify(objToReturn)   

  def verificacionUsuario(self, email, password):
    session = session_factory()
    User_query = session.query(User)
    Obj_usuarios = ''
    Obj_usuario_tipo = ''
    Obj_junta_vecinal = ''
    try:
      Obj_usuarios = User_query.filter((User.email.like(email))).first()
      if not Obj_usuarios:
        session.close()
        return jsonify({"status": 'error', "message": 'email no registrado'})   
      elif (not Obj_usuarios.estado):
        session.close()
        return jsonify({"status": 'error', "message": 'email deshabilitado'})   
      else:
        Obj_usuarios = User_query.filter(and_(User.email.like(email), User.password.like(password))).one()
        Obj_junta_vecinal = session.query(JuntaVecinal).filter(JuntaVecinal.id == Obj_usuarios.junta_vecinal_id).one()
        Obj_usuario_tipo = session.query(UserTipo).filter(UserTipo.id == Obj_usuarios.user_tipo_id).one()
        print(Obj_usuario_tipo.tipo)

        print(Obj_usuarios.fecha_inicio)        
        Obj_usuarios.fecha_inicio = datetime.now()
        #admin = User.query.filter_by(id=Obj_usuarios.id).update(dict(fecha_inicio=datetime.now()))
        session.commit()
        Obj_usuarios = User_query.filter(and_(User.email.like(email), User.password.like(password))).one()
        Obj_usuario_tipo = session.query(UserTipo).filter(UserTipo.id == Obj_usuarios.user_tipo_id).one()
        Obj_junta_vecinal = session.query(JuntaVecinal).filter(JuntaVecinal.id == Obj_usuarios.junta_vecinal_id).one()
        session.close()
      
    except NoResultFound:
      session.close()
      return jsonify({"status": 'error', "message": 'clave incorrecta'})
    except: 
      return jsonify({"status": "error", "message": 'error desconocido en método - verificacionUsuario'})       
    
    return jsonify({"status": 'ok', "message": "usuario verificado", "id": Obj_usuarios.id, "rol": Obj_usuario_tipo.tipo, "junta_vecinal": Obj_junta_vecinal.nombre})   

  def creacionAnuncio(self, body):
    session = session_factory()
    User_query = session.query(User)
    Obj_usuario = ''
    try:
      #filtrar por ID
      Obj_usuario = User_query.filter((User.id == (body['id']))).first()
      if not Obj_usuario:
        session.close()
        return jsonify({"status": 'error', "message": 'id no registrado'})   
      elif (not Obj_usuario.estado):
        session.close()  
        return jsonify({"status": 'error', "message": 'email deshabilitado'})   
      else:
        fecha_creacion = datetime.now()
        nuevoAnuncio = Anunziao(body['monto'], body['titulo'], body['descripcion'], fecha_creacion, Obj_usuario)
        TablaAnunzia = Anunziao_Comentario(nuevoAnuncio)
        session.add(nuevoAnuncio)    
        session.add(TablaAnunzia)

        try:
          arrCategorias = body['categorias']
          if len(arrCategorias) > 0:
            for categoria in arrCategorias:
              Tipo_query = session.query(Tipo)
              Obj_categoria = Tipo_query.filter(Tipo.tipo_name.like(categoria)).first()
              if Obj_categoria:
                Obj_categoria.suscripcion.append(nuevoAnuncio)
        except:
          session.close()
          return jsonify({"status": "error", "message" : 'Error en la inscripcion de categoria a un anuncio'})   
          
            
        session.commit()            
        session.close()          
    except NoResultFound:
      session.close()
      return jsonify({"status": "error", "message": 'error en creacionAnuncio'})   
    except: 
      return jsonify({"status": "error", "message": 'error desconocido en método - creacionAnuncio'})     
    return jsonify({"status": 'ok'})  

  def obtenerUnAnuncio(self, id):
      session = session_factory()
      print("id => ", id)
      Obj_Anunziao = ''
      Obj_reponse = ''
      try:
        Obj_Anunziao = session.query(Anunziao).filter(Anunziao.id_anuncio == id).first()
        if not Obj_Anunziao:
          session.close()
          return jsonify({"status": 'error', "message": 'Anunziao no encontrado'})   
        elif (not Obj_Anunziao.estado):
          session.close()  
          return jsonify({"status": 'error', "message": 'Anunziao deshabilitado'})   
        else:
          #debo extraer toda la data y conformar la respuesta.
          #considerar la oferta actual, comentarios y categorias
          objeto_categoria = Obj_Anunziao.categorias
          arrCategorias = []
          for categoria in objeto_categoria:
            arrCategorias.append(categoria.tipo_name)
            #print("categoria.tipo_name => ",categoria.tipo_name)
          objeto_comentarios = Obj_Anunziao.anunziao_comentario.comentarios
          # acá debo recorrer el nombre del usuario del comentario, la fecha y el comentario 
          # adjuntar cada comentario en otro arreglo
          print("objeto_comentarios =>", objeto_comentarios)

          Obj_Anunziao.visualizaciones = Obj_Anunziao.visualizaciones + 1
          # luego de contruir el especio oferta debo crear: 
          # a si esta oferta se encuentra vigente
          # añadir un campo de oferta existente
          # y añadir el precio final al monto
          arrComentarios = []
          promo= {}
          for promocion in Obj_Anunziao.promociones:
            if promocion.estado == True:
              promo = { "fecha_termino": promocion.fecha_termino, "porcentaje": promocion.procentaje}

          # hay que crear un fecha del comentario
          for comentario in Obj_Anunziao.anunziao_comentario.comentarios:
            Obj_usuario_tipo_query = session.query(User).filter(User.id == comentario.owner_id).one()
            objComentario = { 
              "comentario": comentario.contenido_comentario, 
              "user": Obj_usuario_tipo_query.nombre,
              "fecha_comentario": comentario.fecha_comentario
            }
            arrComentarios.append(objComentario)

          Obj_reponse = { 
            "monto": Obj_Anunziao.monto, 
            "titulo": Obj_Anunziao.titulo,
            "descripcion": Obj_Anunziao.descripcion,
            "nombre_del_publicador": Obj_Anunziao.user.nombre,
            "id_user_publicador": Obj_Anunziao.user.id,
            "categorias": arrCategorias, 
            "fecha_creacion": Obj_Anunziao.fecha_creacion,
            "comentarios": arrComentarios,
            "promocion": promo,
            "telefono": Obj_Anunziao.user.telefono
          }

          session.commit()            
          session.close()          
      except NoResultFound:
        session.close()
        return jsonify({"status": 'error en creacionAnuncio'})
      except: 
        return jsonify({"status": "error", "message": 'error desconocido en método - obtenerUnAnuncio'})     
      return jsonify({"status": 'ok', "anunziao": Obj_reponse}) 

  def todosLosAnunzios(self, switch = False):
    session = session_factory()
    Obj_Anunziao = ''
    Obj_reponse = ''
    arrAnunziaos = []
    try:
      Obj_Anunziao = session.query(Anunziao).filter(Anunziao.estado == True).all()
      if not Obj_Anunziao:
        session.close()
        return jsonify({"status": 'error', "message": 'no hay anuncios'}) 
      else:
        #debo extraer toda la data y conformar la respuesta.
        #considerar la oferta actual, comentarios y categorias
        for anunziao in Obj_Anunziao:

          promo = {}
          for promocion in anunziao.promociones:
            if promocion.estado == True:
              promo = { "fecha_termino": promocion.fecha_termino, "porcentaje": promocion.procentaje}

          objeto_categoria = anunziao.categorias
          arrCategorias = []
          for categoria in objeto_categoria:
            arrCategorias.append(categoria.tipo_name)

          #añadir un email de contacto 
          #añadir un numero de telefono

          Obj_reponse = {
            "monto": anunziao.monto, 
            "titulo": anunziao.titulo,
            "descripcion": anunziao.descripcion,
            "id": anunziao.id_anuncio,
            "categorias": arrCategorias,
            "id_user_publicador": anunziao.user.id, 
            "fecha_creacion": anunziao.fecha_creacion,
            "promocion": promo
          }

          arrAnunziaos.append(Obj_reponse)

        session.commit()            
        session.close()          
    except NoResultFound:
      session.close()
      return jsonify({"status": 'error en obtencion de todos los anunziaos'})   
    except: 
      return jsonify({"status": "error", "message": 'error desconocido en método - todosLosAnunzios'})    
    if switch:
      return arrAnunziaos
    else:
      return jsonify({"status": 'ok', "anunziaos": arrAnunziaos}) 

  def agregarComentarioAnunziao(self, id_anuncio, id_usuario, comentario):
    session = session_factory()
    Obj_Anunziao = []
    try:
      Obj_Anunziao = session.query(Anunziao).filter(and_(Anunziao.id_anuncio == id_anuncio, Anunziao.estado == True)).one()
      UsuarioComenta = session.query(User).filter(User.id == id_usuario).first()
      if not Obj_Anunziao:
        session.close()
        return jsonify({"status": 'error', "message": 'no hay anuncio'}) 
      elif not UsuarioComenta:
        session.close()  
        return jsonify({"status": 'error', "message": "usuario no existe"})
      else:
        print("Obj_Anunziao.titulo => ", Obj_Anunziao.titulo)
        fecha_comentario = datetime.now()
        tabla_comentario_anunziao = Obj_Anunziao.anunziao_comentario        
        elComentario = Comentario(fecha_comentario, comentario, tabla_comentario_anunziao, UsuarioComenta)
        session.add(elComentario)
        #debo extraer toda la data y conformar la respuesta.
        #considerar la oferta actual, comentarios y categorias
        session.commit()            
        session.close()          
    except NoResultFound:
      session.close()
      return jsonify({"status": "error", "message": 'error en la obtencion del anunziao'})  
    except: 
      return jsonify({"status": "error", "message": 'error desconocido en método - agregarComentarioAnunziao'})    
    return jsonify({"status": 'ok', "comentado": True}) 

  def addPromocionAnunziao(self, id_usuario, id_anuncio, porcentaje, duracion_dias):
    session = session_factory()
    Obj_Anunziao = []
    try:
      Obj_Anunziao = session.query(Anunziao).filter(and_(Anunziao.id_anuncio == id_anuncio, Anunziao.estado == True)).one()
      Usuario_owner_anunziao = session.query(User).filter(User.id == id_usuario).first()
      if not Obj_Anunziao:
        session.close()
        return jsonify({"status": 'error', "message": 'no hay anuncio'}) 
      elif not Usuario_owner_anunziao:  
        session.close()  
        return jsonify({"status": 'error', "message": "usuario no encontrado"})
      elif not Usuario_owner_anunziao.estado:
        session.close()  
        return jsonify({"status": 'error', "message": "usuario deshabilitado"})
      else:
        if Obj_Anunziao.user_id == Usuario_owner_anunziao.id:
          for promocion in Obj_Anunziao.promociones:
            if promocion.estado == True:
              promocion.estado = False
          fecha_inicio = datetime.now()
          caduca_anuncio = fecha_inicio + timedelta(days=int(duracion_dias))
          promocion = Promocion(fecha_inicio, caduca_anuncio, porcentaje, Obj_Anunziao)
          print("Obj_Anunziao.titulo => ", Obj_Anunziao.titulo)
          session.add(promocion)
        else:
          session.close()  
          return jsonify({"status": "error", "message": 'el usuario no es el owner del anunziao'})   
        
        session.commit()            
        session.close()          
    except NoResultFound:
      session.close()
      return jsonify({"status": "error", "message": 'error en la obtencion del anunziao'})   
    except: 
      return jsonify({"status": "error", "message": 'error desconocido en método - addPromocionAnunziao'})   
    return jsonify({"status": 'ok', "promocion": True}) 
    
  def todosLasCategorias(self):    
    session = session_factory()
    todas_las_categorias = session.query(Tipo).all()

    arrCategoria = []

    try:

      for categoria in todas_las_categorias:
        print("Categoria de nombre: ", categoria.tipo_name)
        arrCategoria.append(categoria.tipo_name)
      
    except NoResultFound:
      session.close()        
      return jsonify({"status": "error", "message": 'no hay categorias'})
    except:
      return jsonify({"status": "error", "message": 'no hay categorias'})
      
    session.close()        
    return jsonify({"status": "ok", "categoria": arrCategoria})   
    
  def eliminarAnunziao(self):
    session = session_factory()
    Obj_Anunziao = []
    try:
      Obj_Anunziao = session.query(Anunziao).filter(Anunziao.estado == True).all()
      #Usuario_owner_anunziao = session.query(User).filter(User.id == id_usuario).first()
      if not Obj_Anunziao:
        session.close()
        return jsonify({"status": 'error', "message": 'no hay anuncios'}) 

      else:
        for anunziao in Obj_Anunziao:          
          diferencia = datetime.now() - anunziao.user.fecha_inicio          
          print(f"{anunziao.user.nombre} de id: {anunziao.user.id} lleva {diferencia.days} dias sin conectar en Anunziao web")
          maximoDiasSinConexion = 365
          if diferencia.days >= maximoDiasSinConexion:
            print(f"se procede a deshabilitar el anunziao de id: {anunziao.id_anuncio} del usuario {anunziao.user.id} por llevar {maximoDiasSinConexion} sin conectarse")
            anunziao.estado = False
            
      session.commit()            
      session.close()          
    except NoResultFound:
      session.close()
      return jsonify({"status": "error", "message": 'error en la obtencion de los anunziaos'})   
    except: 
      return jsonify({"status": "error", "message": 'error desconocido en método - eliminarAnunziao'})   
    return jsonify({"status": 'ok'}) 

  def poblarBD(self):
    session = session_factory()
    admin = Admin("pablo.lan.tus@gmail.com", "123456")
    session.add(admin)

    ciudadano = UserTipo("ciudadano")
    session.add(ciudadano)
    #
    junta_vecinal = UserTipo("junta_vecinal")
    session.add(junta_vecinal)

    VillaItalia = JuntaVecinal('Villa-Italia')
    session.add(VillaItalia)
    llolleo = JuntaVecinal('llolleo')
    session.add(llolleo)
    santiagoCentro = JuntaVecinal('santiago centro')
    session.add(santiagoCentro)

    Agro = Tipo(tipo_name = 'Agro')
    session.add(Agro)
    Alimentos_y_Bebidas = Tipo(tipo_name = 'Alimentos y Bebidas')
    session.add(Alimentos_y_Bebidas)
    Accesorios_para_Vehículos = Tipo(tipo_name = 'Accesorios para Vehículos')
    session.add(Accesorios_para_Vehículos)
    Animales_y_Mascotas = Tipo(tipo_name = 'Animales y Mascotas')
    session.add(Animales_y_Mascotas)
    Antiguedades_y_Colecciones = Tipo(tipo_name = 'Antiguedades y Colecciones')
    session.add(Antiguedades_y_Colecciones)
    Arte_Libreria_y_Cordoneria = Tipo(tipo_name = 'Arte, Libreria y Cordoneria')
    session.add(Arte_Libreria_y_Cordoneria)
    Autos_Motos_y_Otros = Tipo(tipo_name = 'Autos, Motos y Otros')
    session.add(Autos_Motos_y_Otros)
    Bebes = Tipo(tipo_name = 'Bebes')
    session.add(Bebes)
    Belleza_y_Cuidado_Personal = Tipo(tipo_name = 'Belleza y Cuidado Personal')
    session.add(Belleza_y_Cuidado_Personal)
    Camaras_y_Accesorios = Tipo(tipo_name = 'Camaras y Accesorios')
    session.add(Camaras_y_Accesorios)
    Celulares_y_Telefonia = Tipo(tipo_name = 'Celulares y Telefonia')
    session.add(Celulares_y_Telefonia)
    Computacion = Tipo(tipo_name = 'Computacion')
    session.add(Computacion)
    Consolas_y_Videojuegos = Tipo(tipo_name = 'Consolas y Videojuegos')
    session.add(Consolas_y_Videojuegos)
    Construccion = Tipo(tipo_name = 'Construccion')
    session.add(Construccion)
    Deportes_y_Fitness = Tipo(tipo_name = 'Deportes y Fitness')
    session.add(Deportes_y_Fitness)
    Electrodomesticos = Tipo(tipo_name = 'Electrodomesticos')
    session.add(Electrodomesticos)

    Electronica_Audio_y_Video = Tipo(tipo_name = 'Electronica, Audio y Video')
    session.add(Electronica_Audio_y_Video)
    Entradas_para_Eventos = Tipo(tipo_name = 'Entradas para Eventos')
    session.add(Entradas_para_Eventos)
    Herramientas = Tipo(tipo_name = 'Herramientas')
    session.add(Herramientas)
    Hogar_y_Muebles = Tipo(tipo_name = 'Hogar y Muebles')
    session.add(Hogar_y_Muebles)
    Industrias_y_Oficinas = Tipo(tipo_name = 'Industrias y Oficinas')
    session.add(Industrias_y_Oficinas)
    Inmuebles = Tipo(tipo_name = 'Inmuebles')
    session.add(Inmuebles)
    Instrumentos_Musicales = Tipo(tipo_name = 'Instrumentos Musicales')
    session.add(Instrumentos_Musicales)

    Juegos_y_Juguetes = Tipo(tipo_name = 'Juegos y Juguetes')
    session.add(Juegos_y_Juguetes)
    Libros_Revistas_y_Comics = Tipo(tipo_name = 'Libros, Revistas y Comics')
    session.add(Libros_Revistas_y_Comics)
    Musica_y_Peliculas = Tipo(tipo_name = 'Musica y Peliculas')
    session.add(Musica_y_Peliculas)
    Relojes_y_Joyas = Tipo(tipo_name = 'Relojes y Joyas')
    session.add(Relojes_y_Joyas)
    Salud_y_quipamiento_Medico = Tipo(tipo_name = 'Salud y Equipamiento Medico')
    session.add(Salud_y_quipamiento_Medico)
    Servicios = Tipo(tipo_name = 'Servicios')
    session.add(Servicios)
    Vestuario_y_Calzado = Tipo(tipo_name = 'Vestuario y Calzado')
    session.add(Vestuario_y_Calzado)
    Otras_Categorias = Tipo(tipo_name = 'Otras Categorias')
    session.add(Otras_Categorias)

    #Villa_italia = JuntaVecinal('Villa-Italia')
    #Barrancas = JuntaVecinal('Barrancas')
    
    fecha_inicio = datetime.now()
    Pablo = User("Pablo", "Catalan", 'pabloignacio@gmail.com', '+56993020245', fecha_inicio, '123456', ciudadano, VillaItalia)
    Sebastian = User("Sebastian", "Villalobos", 'sebastian@gmail.com', '+56992210755', fecha_inicio, '123456', junta_vecinal, llolleo)
    Ignacio = User("Ignacio", "Matus", 'Ignacio@gmail.com', '+56993420115', fecha_inicio, '123456', ciudadano, llolleo)
    session.add(Pablo)
    session.add(Sebastian)
    session.add(Ignacio)
    
    fecha_creacion = datetime.now()
    Elanunziao = Anunziao('75990', 'Nuevo headphone con cancel noise', 'Audifonos recargables con duración de 32 horas con sólo tiempo de carga de 1 hora', fecha_creacion, Pablo)
    Elanunziao2 = Anunziao('859990', 'cosplay stormtrooper', 'Traje militar de stormtrooper antes de la venganza de los sith', fecha_creacion, Sebastian)
    
    TablaComentario = Anunziao_Comentario(Elanunziao)
    session.add(Elanunziao)    
    session.add(TablaComentario)

    TablaComentario2 = Anunziao_Comentario(Elanunziao2)
    session.add(Elanunziao2)    
    session.add(TablaComentario2)

    Electronica_Audio_y_Video.suscripcion.append(Elanunziao)
    Consolas_y_Videojuegos.suscripcion.append(Elanunziao)
    Musica_y_Peliculas.suscripcion.append(Elanunziao2)
    Vestuario_y_Calzado.suscripcion.append(Elanunziao2)


    fecha_comentario = datetime.now()
       
    elComentario = Comentario(fecha_comentario, 'Hola, Cuánto pesa este headphone? gracias', TablaComentario, Sebastian)
    elComentario2 = Comentario(fecha_comentario, 'Hace rebaja?', TablaComentario, Sebastian)
    session.add(elComentario)
    session.add(elComentario2)

    elComentario3 = Comentario(fecha_comentario, 'Para que estatura es el traje?', TablaComentario2, Pablo)
    elComentario4 = Comentario(fecha_comentario, 'envía a región el producto?', TablaComentario2, Pablo)
    session.add(elComentario3)
    session.add(elComentario4)

    fecha_inicio = datetime.now()
    caduca_anuncio = fecha_inicio + timedelta(days=int(3))
    promocion = Promocion(fecha_inicio, caduca_anuncio, '0.30', Elanunziao2)
    session.add(promocion)
    caduca_anuncio2 = fecha_inicio + timedelta(days=int(5))
    promocion2 = Promocion(fecha_inicio, caduca_anuncio2, '0.18', Elanunziao)
    session.add(promocion2)

    session.commit()
    session.close()
    return jsonify({"status":"ok"})   

  def crearMunicipioAdmin(self):
    session = session_factory()    
    try:
      fecha = datetime.now()

      san_antonio = Municipio_Tipo("San Antonio")
      session.add(san_antonio)

      municipio = Municipio("pablo", "catalan", "pablo.lan.tus3@gmail.com", "123456", fecha, san_antonio)
      session.add(municipio)
      
      junta_vecinal_query = session.query(JuntaVecinal)
      todas_las_juntas_vecinales = junta_vecinal_query.all()
      todas_las_juntas_vecinales[0].suscripcion.append(municipio)
      todas_las_juntas_vecinales[1].suscripcion.append(municipio)
      todas_las_juntas_vecinales[2].suscripcion.append(municipio)
      session.commit()
    except IntegrityError:
      print('Acá se encuentra el error')

    
    session.close()

    return jsonify({"status":"ok"})   

  def rankingCiudadanos(self):
    arrUserRanking = []
    arrTodosLasPublicaciones = self.todosLosAnunzios(True)
    arrUserReview = []
    session = session_factory()
    # municipio_Tipo_query = session.query(Municipio_Tipo)
    # junta_vecinal_query = session.query(JuntaVecinal)

    


    # todas_las_juntas_vecinales = junta_vecinal_query.all()
    # todos_los_municipios = municipio_Tipo_query.all()

    # for juntaVecinal in todas_las_juntas_vecinales:
    #   print('juntaVecinal.nombre =>',juntaVecinal.nombre)
    # for municipio in todos_los_municipios:
    #   print('nombre_tipo => ', municipio.nombre_tipo)

    # arrUserByjuntaVecinal = []
    # for juntaVecinal in todas_las_juntas_vecinales:
    #   objToSave = {
    #     "users": juntaVecinal.users,
    #     "junta_vecinal": juntaVecinal.nombre
    #   }
    #   arrUserByjuntaVecinal.append(objToSave)

    # print("arrUserByjuntaVecinal => ", arrUserByjuntaVecinal)
    
    # fecha_inicio = datetime.now()
    # fecha_hasta = fecha_inicio - timedelta(days=int(7))

    # while fecha_hasta <= fecha_inicio:

    #   for juntaVecinal in arrUserByjuntaVecinal:
    #     fecha_hasta = fecha_hasta + timedelta(days=int(1))



    #número total de anuncios realizados durante el día
    today = date.today()    
    fechaDuranteXdia = datetime.now()    
    f'{today.year}-{today.month}-{today.day}'
    fecha_inicio = datetime.now()
    fecha_inicio = fecha_inicio - timedelta(days=int(1))
    f'{fecha_inicio.year}-{fecha_inicio.month}-{fecha_inicio.day}'
    anunciosXdia = session.query(Anunziao).filter(and_(Anunziao.fecha_creacion >= fecha_inicio, Anunziao.fecha_creacion <= fechaDuranteXdia, Anunziao.estado == True)).all()
    
    if len(anunciosXdia) > 0:        
      print("anuncios realizados durante el día ->", len(anunciosXdia))



    # un ranking de las juntas vecinales con más publicaciones por día
    obj_Municipio_tipo = session.query(Municipio_Tipo).filter(Municipio_Tipo.nombre_tipo == 'San Antonio').one()
    municipios = obj_Municipio_tipo.municipios
    juntas_vecinales = ''
    for municipio in municipios:
      juntas_vecinales = municipio.junta_vecinales

    arrJuntas_vecinales = []

    for juntaVecinal in juntas_vecinales:
      arrJuntas_vecinales.append(juntaVecinal.nombre)

    print('arrJuntas_vecinales =>', arrJuntas_vecinales)
    anunciosXdia = session.query(Anunziao).filter(and_(Anunziao.fecha_creacion >= fecha_inicio, Anunziao.fecha_creacion <= fechaDuranteXdia, Anunziao.estado == True)).all()
    ranking_publicacion_por_juntaVecinal = []
    for junta_vecinal in arrJuntas_vecinales:      
      contadorJuntaVecinal = 0
      for anuncio in anunciosXdia:
        if anuncio.user.junta_vecinal.nombre == junta_vecinal:
          contadorJuntaVecinal += 1
      jsonToSaveRankingJuntaVecinal = { 
        'junta_vecinal': junta_vecinal,
        'publicaciones': contadorJuntaVecinal
      }
      ranking_publicacion_por_juntaVecinal.append(jsonToSaveRankingJuntaVecinal)
    
    print("ranking_publicacion_por_juntaVecinal => ", ranking_publicacion_por_juntaVecinal)


    #Un ranking de los ciudadanos que más anuncios publican en su comuna
    isDB = False

    for publicacion in arrTodosLasPublicaciones:
      existeRegistroUserRanking = False
      if len(arrUserReview) > 0:
        for id in arrUserReview:
          if isDB:
            if id.get("id") == publicacion.user.id:
              existeRegistroUserRanking = True
          else:
            if id.get("id") == publicacion.get("id_user_publicador"):
              existeRegistroUserRanking = True

      contador = 1
      
      if not existeRegistroUserRanking:
        if isDB:
          objeto1 = {
            "id": publicacion.user.id
          }
        else:
          objeto1 = {
            "id": publicacion.get("id_user_publicador")
          }

        arrUserReview.append(objeto1)
        for publicacion2 in arrTodosLasPublicaciones:
          if isDB:
            if publicacion.user.id == publicacion2.user.id and publicacion.id_anuncio != publicacion2.id_anuncio:
              contador += 1
          else:
            if publicacion.get("id_user_publicador") == publicacion2.get("id_user_publicador") and publicacion.get("id") != publicacion2.get("id"):
              contador += 1
        if isDB:
          user = session.query(User).filter(User.id == publicacion.user.id).one()
        else:
          user = session.query(User).filter(User.id == publicacion.get("id_user_publicador")).one()
        
        objeto2 = {
          "nombre": user.nombre, 
          "email": user.email,
          "user_id": publicacion.user.id if isDB else publicacion.get("id_user_publicador"), 
          "publicaciones": contador
        }
        arrUserRanking.append(objeto2)
        
    return jsonify({"status": "ok", "ranking": arrUserRanking})

  def todasLasJuntasVecinales(self):

    session = session_factory()
    todas_las_categorias = ''

    arrJuntasVecinales = []

    try:
      todas_las_categorias = session.query(JuntaVecinal).all()
      for categoria in todas_las_categorias:
        print("Juntas Vecinales de nombre: ", categoria.nombre)
        arrJuntasVecinales.append(categoria.nombre)
      
    except NoResultFound:
      session.close()        
      return jsonify({"status": "error", "message": 'no hay junta Vecinales'})
    except:
      return jsonify({"status": "error", "message": 'no hay junta Vecinales'})
      
    session.close()        
    return jsonify({"status": "ok", "categoria": arrJuntasVecinales})   
#duplicate key value violates unique constraint
    
    