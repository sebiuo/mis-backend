from flask_marshmallow import Marshmallow

ma = Marshmallow()

class AnunziaoAllResponse(ma.Schema):
  class Meta:
    fields = ('id','descripcion','titulo', 'monto')
