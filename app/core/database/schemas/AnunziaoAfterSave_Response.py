from flask_marshmallow import Marshmallow

ma = Marshmallow()

class AnunziaoCreateResponse(ma.Schema):
  class Meta:
    fields = ('id','nombre','apellido','email', 'url')