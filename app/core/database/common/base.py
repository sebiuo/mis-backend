from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine('postgresql://usr:pass@db:5432/sqlalchemy')
#engine = create_engine('postgresql://usr:pass@159.203.106.137:5432/sqlalchemy')
#engine = create_engine('postgresql://usr:pass@db:5432/sqlalchemy')

# use session_factory() to get a new Session
_SessionFactory = sessionmaker(bind=engine)

Base = declarative_base()

def session_factory():
    Base.metadata.create_all(engine)
    return _SessionFactory()
