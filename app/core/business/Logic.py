from app.core.database.Controller import Controller
from flask import jsonify, make_response

class Logic:
  def __init__(self):
    pass

  def registerJuntaVecinalUser(self, body):
    isTrue = Controller().isAdmin(body['emailAdmin'], body['passwdAdmin'])
    if isTrue:
      objetoResponse = Controller().crearUsuarioPriveliago(body['nombre'], body['apellido'], body['email'], body['telefono'], body['password'], body['nombre_junta_vecinal'])
    else:
      return jsonify({"status": "error", "message": "admin no encontrado"})
    
    return objetoResponse

  def poblarBaseDeDatos(self, body):
    objetoResponse = Controller().poblarBD()
    return objetoResponse
  
  def inscripcionCiudadano(self, body):
    #acá podriamos evaluar el token - esto será en un método aparte - se rescatará del token el tipo de usuario
    #los parametros de entradas correctos
    isTrue = Controller().isJuntaVecinal(body['emailJuntaVecinal'], body['passwdJuntaVecinal'])
    if isTrue:
      objetoResponse = Controller().crearUsuario(body['nombre'], body['apellido'], body['email'], body['telefono'],body['password'], body['nombre_junta_vecinal'])
    else:
      return jsonify({"status": "error", "message": "email o contraseña del personal de la junta vecinal no coincide"})
    #aca podemos refrescar el token
    #añadir data como respuesta
    return objetoResponse
  
  def inicioSesion(self, body):
    objetoResponse = Controller().verificacionUsuario(body['email'], body['password'])
    #aca podemos refrescar el token
    #añadir data como respuesta
    return objetoResponse
  
  def nuevoAnuncio(self, body):
    objetoResponse = Controller().creacionAnuncio(body)
    #aca podemos refrescar el token
    #añadir data como respuesta
    return objetoResponse
  
  def seleccionarAnunziao(self, body):
    objetoResponse = Controller().obtenerUnAnuncio(body['id'])
    return objetoResponse

  def obtenerAnunziaos(self):
    objetoResponse = Controller().todosLosAnunzios()
    return objetoResponse

  def comentarAnunziao(self, body):
    objetoResponse = Controller().agregarComentarioAnunziao(body['id_anuncio'], body['id_usuario'], body['comentario'])
    return objetoResponse 
  
  def crearPromocion(self, body):
    objetoResponse = Controller().addPromocionAnunziao(body['id_usuario'], body['id_anuncio'], body['porcentaje'], body['duracion_dias'])
    return objetoResponse 
  
  def actualizarEstadoAnunziaos(self):
    objetoResponse = Controller().eliminarAnunziao()
    return objetoResponse 

  def obtenerCategorias(self):
    objetoResponse = Controller().todosLasCategorias()
    return objetoResponse

  def crearMunicipioAdmin(self):
    objetoResponse = Controller().crearMunicipioAdmin()
    return objetoResponse
  
  def obtenerTodasLasJuntasVecinales(self):
    objetoResponse = Controller().todasLasJuntasVecinales()
    return objetoResponse
  
  def obtenerRankingCiudadanos(self):
    objetoResponse = Controller().rankingCiudadanos()
    return objetoResponse