from unittest import TestCase
from app.core.business.Logic import Logic
from functionsMock import user_register, user_login, user_login_fail

class TestResponse(TestCase):

    def test_unit_1(self):
        respuesta = Logic().createResponse()
        expected = 'Esto es un string'
        assert expected == respuesta        

    def test_unit_2(self):        
        respuesta = Logic().createResponse2()        
        expected = 'Esto es un string2'
        self.assertEqual(expected, respuesta)

    def test_unit_3(self):        
        respuesta = Logic().createResponse3()        
        expected = 'Esto es un string3'
        self.assertEqual(expected, respuesta)

    def test_unit_4(self):
        api_result = user_register()
        expected = [{
            "apellido": "Morales",
            "email": "guillermo@gmail.com",
            "password": "123123123",
            "id": 1,
            "nombre": "Guillermo",
            }]
        self.assertEqual(api_result, expected)

    def test_unit_5(self):
        api_result = user_login()
        expected = [{
            "id": 1,
            "nombre": "guillermo@gmail.com",
            "status": "ok"
            }]
        self.assertEqual(api_result, expected)

    def test_unit_6(self):
        api_result = user_login_fail()
        expected = [{
            "message": "user not exist",
            "nombre": "guillermo.morales@gmail.com",
            "status": "error"
            }]
        self.assertEqual(api_result, expected)