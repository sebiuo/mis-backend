from flask import Blueprint, current_app, request
import json
from werkzeug.local import LocalProxy
from app.core.business.Logic import Logic
from authentication import require_appkey
from flask_sqlalchemy import SQLAlchemy

core = Blueprint('core', __name__)
logger = LocalProxy(lambda: current_app.logger)

db = SQLAlchemy()
@core.before_request
def before_request_func():
  current_app.logger.name = 'core'

@core.route('/registerJuntaVecinalUser', methods=['POST'])
@require_appkey
def registerJuntaVecinalUser():
  response = Logic().registerJuntaVecinalUser(request.json)
  return response

@core.route('/poblarBaseDeDatos', methods=['POST'])
@require_appkey
def poblarBaseDeDatos():
  response = Logic().poblarBaseDeDatos(request.json)
  return response

@core.route('/inscripcionCiudadano', methods=['POST'])
@require_appkey
def inscripcionCiudadano():
  response = Logic().inscripcionCiudadano(request.json)
  return response

@core.route('/inicioSesion', methods=['POST'])
@require_appkey
def inicioSesion():
  response = Logic().inicioSesion(request.json)
  return response

@core.route('/nuevoAnuncio', methods=['POST'])
@require_appkey
def nuevoAnuncio():
  response = Logic().nuevoAnuncio(request.json)
  return response

@core.route('/seleccionarAnunziao', methods=['POST'])
@require_appkey
def seleccionarAnunziao():
  response = Logic().seleccionarAnunziao(request.json)
  return response

@core.route('/obtenerAnunziaos', methods=['POST'])
@require_appkey
def obtenerAnunziaos():
  response = Logic().obtenerAnunziaos()
  return response

@core.route('/comentarAnunziao', methods=['POST'])
@require_appkey
def comentarAnunziao():
  response = Logic().comentarAnunziao(request.json)
  return response

@core.route('/crearPromocion', methods=['POST'])
@require_appkey
def crearPromocion():
  response = Logic().crearPromocion(request.json)
  return response

@core.route('/actualizarEstadoAnunziaos', methods=['POST'])
@require_appkey
def actualizarEstadoAnunziaos():
  response = Logic().actualizarEstadoAnunziaos()
  return response

@core.route('/obtenerCategorias', methods=['GET'])
@require_appkey
def obtenerCategorias():
  response = Logic().obtenerCategorias()
  return response

@core.route('/crearMunicipioAdmin', methods=['POST'])
@require_appkey
def crearMunicipioAdmin():
  response = Logic().crearMunicipioAdmin()
  return response

@core.route('/obtenerTodasLasJuntasVecinales', methods=['GET'])
@require_appkey
def obtenerTodasLasJuntasVecinales():
  response = Logic().obtenerTodasLasJuntasVecinales()
  return response

@core.route('/obtenerRankingCiudadanos', methods=['GET'])
@require_appkey
def obtenerRankingCiudadanos():
  response = Logic().obtenerRankingCiudadanos()
  return response
  
  


@core.route('/restricted', methods=['GET'])
@require_appkey
def restricted():
  return 'Congratulations! Your core-app restricted route is running via your API key!'
