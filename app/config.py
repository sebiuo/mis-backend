from os import environ, path
from dotenv import load_dotenv

basedir = path.abspath(path.join(path.dirname(__file__), '..'))
load_dotenv()

class BaseConfig(object):
  APP_NAME = environ.get('APP_NAME') or 'flask-boilerplate'
  ORIGINS = ['*']
  EMAIL_CHARSET = 'UTF-8'
  API_KEY = environ.get('API_KEY')

class Development(BaseConfig):
  DEBUG = True
  ENV = 'dev'

class Staging(BaseConfig):
  DEBUG = True
  ENV = 'staging'

class Production(BaseConfig):
  DEBUG = False
  ENV = 'production'

config = {
  'development': Development,
  'staging': Staging,
  'production': Production,
}
